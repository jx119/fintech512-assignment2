# Coding the song: 99 Bottles 

Approach: Write code to pass the tests for 99 bottles

1. Fork this repo.
2. Ensure that you can build the code and run the tests. The repo has a single passing test.
3. Once you can build and run the code, set a two hour timer, and continue with the following steps.
4. Remove the '@Disabled("Remove this line to add this test.")' line from the next test in BottlesTest.Java
5. Write the code to pass the test enabled in step 4.
6. When the code passess all enabled tests, commit your changes. The commit message should name the test that was passed.
7. Return to step 4, until you pass all tests, or two hours have passed.
  
- Last commit: add difficulties for commit. It is written on the last line of the assignment instruction where it is very likely to be ignored.