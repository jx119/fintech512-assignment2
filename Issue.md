# 99 Bottles Assignment
## Project Requirements
1. Fork the [99 bottles assignment repo] and make sure you can run the tests already provided for you.  You may also fork the maven-based project repo at https://gitlab.oit.duke.edu/fintech-512/spring-2022/fintech512-assignment2 as an alternative.
2. Read the PSP script and follow the directions there for documenting your work on the 99 bottles program.
3. Write Java code to pass each of the tests provided in the repo. For this step, plan for when you can spend two hours coding, and spend the two hours working only on this part of the assignment. When you have used the two hours (or less, if you finish before two hours), you are done with this part of the assignment.
4. Each time you get a new test to pass, commit your code changes to your GitLab repo. In the commit message, briefly (e.g., one sentence) describe any difficulties you had.

## Instructions:
1. Fork this repo.
1. Ensure that you can build the code and run the tests. The repo has a single passing test.
1. Once you can build and run the code, set a two hour timer, and continue with the following steps.
1. Remove the '@Disabled("Remove this line to add this test.")' line from the next test in BottlesTest.Java
1. Write the code to pass the test enabled in step 4.
1. When the code passes all enabled tests, commit your changes. The commit message should name the test that was passed.
1. Return to step 4, until you pass all tests, or two hours have passed.

## Planning
**_Create a conceptual design of the components needed to implement the required functionality._**
- The program requires three functions
> `public String verse(int verseNumber)`  
> `public String verse(int startVerseNumber, int endVerseNumber)`  
> `public String song()`  

function verse() with two parameter calls function verse() with one parameter, and function song() calls verse(start, end)

**_Estimate the size of each component, in lines of code_**
- `verse(int verseNumber)` 10 lines
- `public String verse(int startVerseNumber, int endVerseNumber)` 5 lines
- `public String song()` 5 lines

**_Make your best estimate of the time required to develop each component, and report both the individual estimates and the total._**
- `fork the repo, build the project and run the tests` 5 min 
- `verse(int verseNumber)` 10 min
- `public String verse(int startVerseNumber, int endVerseNumber)` 5 min 
- `public String song()` 5 min
- total: 25 min

**_Time took to make the plan_**
- 30 min

## Development
_**Add a ‘Development’ comment on the issue and record your development time**_
- `fork the repo, build the project and run the tests` _**60 min**_
- `verse(int verseNumber)` 10 min
- `public String verse(int startVerseNumber, int endVerseNumber)` 5 min
- `public String song()` 5 min
> the gradle project is incompatible and unable to build. Not fixed until the assignment had an update with Maven project.

## Testing
_**When testing is completed, add a ‘Testing’ comment on the issue to record your testing time as a comment on the ‘Testing’ issue.**_
- 10 min 
> It is a relatively simple program and no defects are found with the given tests.

